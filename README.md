# pipelines-php-mysql

[Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines) [Docker](https://www.docker.com/)

Image based on [Debian/Jessie](https://www.debian.org/releases/jessie/) and [smartapps/bitbucket-pipelines-php-mysql](https://hub.docker.com/r/smartapps/bitbucket-pipelines-php-mysql)

More help in Bitbucket's [Confluence](https://confluence.atlassian.com/bitbucket/bitbucket-pipelines-beta-792496469.html)

## Packages installed

 - `php-cli`, `php-sqlite`, `php-mysqlnd`, `php-mcrypt`, `php-curl`, `php-gettext`, `php-gd`, `php-json`, `php-intl`, `php-xdebug`, `php-imagick`, `php-memcached`, `memcached`, `imagemagick`, `openssh-client`, `curl`, `gettext`, `zip`, `unzip`, `mariadb-server`, `mariadb-client`, `git`
 - [MariaDB](https://mariadb.com/) 10.1 (user `root:root`)
 - [PHP](http://php.net/) 7.1
 - [Node.js](https://nodejs.org/) 4.x LTS
 - Latest [Composer](https://getcomposer.org/), [Gulp](http://gulpjs.com/), [Webpack](https://webpack.github.io/), [Mocha](https://mochajs.org/), [Grunt](http://gruntjs.com/), [PHPUnit](https://phpunit.de/), [Codeception](https://codeception.com/)

## Sample `bitbucket-pipelines.yml`

```YAML
image: senyahnoj/pipeline-php-mysql
pipelines:
  default:
    - step:
        script:
          - service mysql start
          - mysql -h localhost -u root -proot -e "CREATE DATABASE test;"
          - composer install --no-interaction --no-progress --prefer-dist
          - npm install --no-spin
          - gulp
```
